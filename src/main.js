import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import lazyload from "./utilities/lazyload";

Vue.config.productionTip = false;
Vue.mixin(lazyload);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
